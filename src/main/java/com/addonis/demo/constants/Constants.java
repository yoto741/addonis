package com.addonis.demo.constants;

/**
 * Constants for exceptions
 */
public class Constants {
    public static final String TAG = "tag";
    public static final String ADDON = "addon";
    public static final String ADDON_A = "Addon";
    public static final String NAME = "name";
    public static final String USER_U = "User";
    public static final String USER = "user";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String URL = "url";
    public static final String EMAIL = "email";
    public static final String DELETE_CONFIRMATION = "Addon was deleted successfully";
    public static final String PASS_NOT_BLANK = "Password must not be blank";
    public static final String NAME_NOT_BLANK = "Name must not be blank";
    public static final String EMAIL_NOT_BLANK = "Email must not be blank";
    public static final String DESCRIPTION_NOT_BLANK = "Description must not be blank";
    public static final String LINK_NOT_BLANK = "Link must not be blank";
    public static final String FILE_NOT_BLANK = "file must not be blank";
    public static final String BINARY = "Binary";
    public static final String PICTURE = "Picture";
    public static final String FAIL_TO_UPLOAD_IMAGE = "Failed to upload image";
}
