package com.addonis.demo.repository.contracts;

import com.addonis.demo.models.Readme;
import com.addonis.demo.repository.base.BaseRepository;

public interface ReadmeRepository extends BaseRepository<Readme, Integer> {
}
