package com.addonis.demo.repository.contracts;

import com.addonis.demo.models.BinaryContent;
import com.addonis.demo.repository.base.BaseRepository;

public interface BinaryContentRepository extends BaseRepository<BinaryContent, Integer> {
}
