package com.addonis.demo.services.contracts;

import com.addonis.demo.models.BinaryContent;
import com.addonis.demo.services.base.BaseServiceContract;

public interface BinaryContentService extends BaseServiceContract<BinaryContent, Integer> {
}
