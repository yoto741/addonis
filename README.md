# Addonis
## Addonis/ Liliya & Yoto

**American Eagle Project**

https://trello.com/b/3X849l4f/addonis

https://addonis.herokuapp.com/

Addonis is a plug-ins registry web application. 
It is a new type of innovative technology showing the pulls count and open issues of the addon's repository on live time.
The problem that our product solves that it gathers add-ons for all types of IDE's in a single place. 
This is the right place to find the right tool for the user's needs. 

Some of the possible actions it enables its users to do are:
* Publish their own addons
* Browse addons for their prefered IDE
* Download addons
* Rate existing addons
* Filter addons by tag
* Sort addons by date/downloads/name ascending or descending order
* Edit their own addons and profile 

**Registration:**

*In order to upload your own addon you need to register in the top right corner as shown in the picture below.*
*For the registration you need to provide the following information: username and password*
![addonis1](/uploads/08ac3c6a43106a77c69da58ebfa362d1/addonis1.png)


**Create addon:**

*To create your own addon you need go to the create addon page as shown in the picture bellow.*
![Screenshot_1](/uploads/b2f9a3e4fe80c69ef151084ae55ca132/Screenshot_1.png)

*The you will be redirected to the addon create form where you need to enter all the information for your addon and upload the addon itself.*
![Screenshot_2](/uploads/ebf0ac20af03f9cf71e9362e066b8d3d/Screenshot_2.png)

*After you have created your addon, it will be in a pending state until it is approved by an admin.*
![Screenshot_3](/uploads/fae362fb6798e402cbb5c54f4b60ef0b/Screenshot_3.png)


**Sorting and Filtering:**

*You have the possibility sort addons by name/last commit date/upload date/downloads in ascending/descending order or filter addons by IDE or Tag*
![Screenshot_4](/uploads/17f58218b01ce082332d110ef0ab04c5/Screenshot_4.png)



## Licenses and copyright
Apache-2.0