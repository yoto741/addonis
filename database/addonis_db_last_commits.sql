INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (10, '2020-04-10 21:00:00', 'w n', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (11, '2020-04-10 21:00:00', 'w n', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (12, '2020-04-10 21:00:00', 'w n', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (13, '2020-04-10 21:00:00', 'w n', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (14, '2020-04-10 21:00:00', 'w n', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (15, '2020-04-10 21:00:00', 'w n', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (16, '2020-04-01 21:00:00', 'Merge pull request #68 from mapitman/master

Fix broken links to Visual Studio gallery', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (17, '2020-04-14 21:00:00', 'Merge pull request #2814 from wangzq/support-update

Support "update" and show which token failed to parse', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (18, '2020-03-27 21:00:00', 'Merge pull request #2460 from github/azure-pipelines/certificate-sha1

Sign the VSIX', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (19, '2019-12-22 21:00:00', 'Update CHANGELOG.md', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (20, '2019-12-22 21:00:00', 'Update CHANGELOG.md', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (21, '2019-09-18 21:00:00', 'Async package (#11)

Patch by Thieum', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (22, '2020-04-17 21:00:00', 'Use a password box for credentials in TranslatorConfiguration', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (23, '2020-04-22 21:00:00', 'attempt to fix #842', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (24, '2019-10-18 21:00:00', 'V4.5.2 2019/10/19
- Improved autoloading.', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (25, '2019-04-10 21:00:00', 'limiting extension support for now', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (26, '2020-04-11 21:00:00', ':up: 3.12.6', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (27, '2020-03-23 21:00:00', 'Merge pull request #289 from tomasr/develop

v4.3 release merge', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (28, '2019-11-21 21:00:00', 'moved oidc dependencies into a seprate service class, minor cleanup', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (29, '2020-04-24 21:00:00', 'updated description', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (30, '2020-04-19 21:00:00', 'Merge branch ''master'' of https://github.com/oufly/Bootstrap4vs', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (31, '2020-04-24 21:00:00', 'updated description', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (32, '2020-04-22 21:00:00', 'rollback Microsoft.VisualStudio.Threading and use VS''s version (#28)', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (33, '2018-05-19 21:00:00', 'Fix for #183 - CopyAsHTMLMarkup font size

Updated GetFontSize to print "px" on font size.', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (34, '2020-04-15 21:00:00', 'Merge pull request #666 from terrajobst/fix-coc-links

Fix links to Code of Conduct', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (35, '2020-04-08 21:00:00', 'Fix incorrect javadoc

The unit is seconds, not milliseconds', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (36, '2019-08-29 21:00:00', 'Merge pull request #53 from jnm2/add_designer_ignore_pattern

Add `.designer.` ignore pattern', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (37, '2020-04-27 21:00:00', '?????????????', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (38, '2020-04-18 21:00:00', 'Merge pull request #738 from nunit/issue735

Issue735', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (39, '2019-08-19 21:00:00', 'Removed need for autoload by using VisibilityConstraints', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (40, '2018-08-06 21:00:00', 'Added advanced visualizer for boost::property_tree', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (41, '2020-03-08 21:00:00', 'Merge pull request #66 from SonarSource/dependabot/maven/security/com.fasterxml.jackson.core-jackson-databind-2.9.10.3

Bump jackson-databind from 2.9.10.1 to 2.9.10.3 in /security
fixes CVE-2020-8840', null);
INSERT INTO addonis_db.last_commits (last_commit_id, date, title, addon_id) VALUES (42, '2020-04-25 21:00:00', '[maven-release-plugin] prepare for next development iteration', null);